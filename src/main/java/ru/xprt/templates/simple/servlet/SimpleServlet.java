package ru.xprt.templates.simple.servlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ru.xprt.templates.simple.HttpSoap;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class SimpleServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setCharacterEncoding(StandardCharsets.UTF_8.name());
//        resp.setContentType("text/html; charset=utf-8");
        
        HttpSoap soap = new HttpSoap();
        String data = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ser=\"http://service.nsandrus.ru/\">"
        		+ "<soapenv:Header/> <soapenv:Body> <ser:getDomains/> </soapenv:Body></soapenv:Envelope>";
		String response = soap.postDataAuthorize(data , 30);
        
		TemplatePage template = new TemplatePage();
		String page = template.getPage(response);
        resp.getWriter().print(page);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding(StandardCharsets.UTF_8.name());
        
        System.out.println(req.getParameter("name"));
        System.out.println(req.getParameter("email"));
        System.out.println(req.getParameter("phone"));
        System.out.println(req.getParameter("message"));
        
        HttpSoap soap = new HttpSoap();
        String data = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ser=\"http://service.nsandrus.ru/\">"
        		+ "<soapenv:Header/> <soapenv:Body> <ser:getDomains/> </soapenv:Body></soapenv:Envelope>";
		String response = soap.postDataAuthorize(data , 30);
        
        resp.getWriter().print(response);
    }
}
