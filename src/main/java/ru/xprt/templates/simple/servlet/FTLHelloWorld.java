package ru.xprt.templates.simple.servlet;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;
import freemarker.template.TemplateNotFoundException;


public class FTLHelloWorld {
	
	public static void main(String[] args) throws IOException {
		
		Configuration cfg = new Configuration(Configuration.VERSION_2_3_25);

		// Specify the source where the template files come from. Here I set a
		// plain directory for it, but non-file-system sources are possible too:
		cfg.setDirectoryForTemplateLoading(new File("/store"));

		// Set the preferred charset template files are stored in. UTF-8 is
		// a good choice in most applications:
		cfg.setDefaultEncoding("UTF-8");

		// Sets how errors will appear.
		// During web page *development* TemplateExceptionHandler.HTML_DEBUG_HANDLER is better.
		cfg.setTemplateExceptionHandler(TemplateExceptionHandler.HTML_DEBUG_HANDLER);

		// Don't log exceptions inside FreeMarker that it will thrown at you anyway:
		cfg.setLogTemplateExceptions(false);
		
		
		// Create the root hash. We use a Map here, but it could be a JavaBean too.
		Map<String, Object> root = new HashMap<>();

		// Put string "user" into the root
		root.put("user", "Big Joe");

		// Create the "latestProduct" hash. We use a JavaBean here, but it could be a Map too.
		Product latest = new Product();
		latest.setUrl("products/greenmouse.html");
		latest.setName("green mouse");
		// and put it into the root
		root.put("latestProduct", latest);
		Template temp = cfg.getTemplate("test.ftlh");
		
		
		
//			String name;
//			//Load template from source folder
//			Map<String, Object> data = new HashMap<String, Object>();
//			try {
//			Template template = cfg.getTemplate("helloworld.ftl");
//			
//			// Build the data-model
//			
//			data.put("message", "Hello World!");
//
//			//List parsing 
//			List<String> countries = new ArrayList<String>();
//			countries.add("India");
//			countries.add("United States");
//			countries.add("Germany");
//			countries.add("France");
//			
//			data.put("countries", countries);
//			
//			
//			// Console output
//			Writer out = new OutputStreamWriter(System.out);
//			template.process(data, out);
//			out.flush();
//
//			
//			} catch (TemplateNotFoundException e1){
//				System.out.println(e1);
//			} catch (TemplateException e2) {
//				
//			}
//			
	
	}
}