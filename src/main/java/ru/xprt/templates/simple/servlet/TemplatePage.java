package ru.xprt.templates.simple.servlet;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;

public class TemplatePage {

	public String getPage(String response) throws IOException {
		
		Configuration cfg = new Configuration(Configuration.VERSION_2_3_25);

		cfg.setDirectoryForTemplateLoading(new File("/store"));
		cfg.setDefaultEncoding("UTF-8");
		cfg.setTemplateExceptionHandler(TemplateExceptionHandler.HTML_DEBUG_HANDLER);
		cfg.setLogTemplateExceptions(false);
	
		try {
		Template template = cfg.getTemplate("test.ftlh");
		
			Map<String, Object> data = new HashMap<String, Object>();
				
	        /* Create a data-model */
	        try {
				data.put(
				        "doc",
				        freemarker.ext.dom.NodeModel.parse(new InputSource(new StringReader(response))));
			} catch (SAXException e) {
				e.printStackTrace();
			} catch (ParserConfigurationException e) {
				e.printStackTrace();
			}

			// write to string
	         StringWriter output = new StringWriter();
	        template.process(data, output);
	        return output.toString();
	        
		} catch (IOException e) {
			e.printStackTrace();
		} catch (TemplateException e) {
			e.printStackTrace();
		}
		return "";
	}

	
	
}
